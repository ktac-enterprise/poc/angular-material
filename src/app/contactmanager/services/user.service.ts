import { Injectable } from '@angular/core';
import {User} from "../models/user";
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable} from "rxjs";

@Injectable()
export class UserService {

  private _users: BehaviorSubject<User[]>;

  private dataStore: {
    users: User[]
  }

  constructor(private http: HttpClient) {
    this.dataStore = {users: []};
    this._users = new BehaviorSubject<User[]>([]);
  }

  get users(): Observable<User[]> {
    return this._users.asObservable();
  }

  loadAll() {
    const userUrl = 'https://angular-material-api.azurewebsites.net/users';
    return this.http.get<User[]>(userUrl)
      .subscribe((data: any) => {
        this.dataStore.users = data;
        this._users.next(Object.assign([{}], this.dataStore.users));
      }, error => {
        console.error(error);
      })
  }

  userById(id: number): User {
    return <User>this.dataStore.users.find(x => x.id == id);
  }

  addUser(user: User): Promise<User> {
    return new Promise((resolve, reject) => {
      user.id = this.dataStore.users.length + 1;
      this.dataStore.users.push(user);
      this._users.next(Object.assign([{}], this.dataStore.users)); // notifier les composant qui on souscrire à cette liste pour qu'ils puissent avoir les mise à jours
      resolve(user);
    });
  }
}
