import {Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {User} from "../../models/user";
import {FormControl, Validators} from "@angular/forms";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-new-contact-dialog',
  templateUrl: './new-contact-dialog.component.html',
  styleUrls: ['./new-contact-dialog.component.scss']
})
export class NewContactDialogComponent implements OnInit {

  avatars = ['svg-1', 'svg-2', 'svg-3', 'svg-4']

  // @ts-ignore
  user: User;
  constructor(private dialogRef: MatDialogRef<NewContactDialogComponent>, private userService: UserService) {
  }

  name = new FormControl('', [Validators.required, Validators.minLength(3)]);
  bio = new FormControl('', [Validators.required, Validators.minLength(50)]);

  ngOnInit(): void {
    this.user = new User();
  }
  save() {
    // @ts-ignore
    this.user.name = this.name.value;
    // @ts-ignore
    this.user.bio = this.bio.value;
    this.userService.addUser(this.user).then(user => {
      this.dialogRef.close(user);
    })
  }

  dismiss() {
    this.dialogRef.close(null);
  }

  getNameErrorMessage() {
    if (this.name.hasError('required')) {
      return 'You must provide a name';
    }

    return this.name.hasError('minlength') ? 'the name should be at least 3 characters' : '';
  }

  getBioErrorMessage() {
    if (this.bio.hasError('required')) {
      return 'You must provide a bio';
    }

    return this.bio.hasError('minlength') ? 'the bio should be at least 50 characters' : '';
  }

}
